/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#version 330 core
layout(location = 0) in vec4 spritePosSizeAngle;
//layout(location = 1) in vec2 spriteScale;
//layout(location = 2) in vec4 spriteColor;
layout(location = 1) in vec4 spriteColor;
uniform mat4 Projection;
uniform uint timeUsec;

mat2 rotate(float angle)
{
    return mat2(cos(angle), -sin(angle),
                sin(angle), cos(angle));
}

out mat2 fSpriteRotateMatrix;
//out vec2 fSpriteScale;
out vec4 fSpriteColor;

void main()
{
    gl_Position = Projection * vec4(spritePosSizeAngle.xy, 0.0, 1.0);
    gl_PointSize = spritePosSizeAngle.z;

    float angle = spritePosSizeAngle.w - float(timeUsec) / 1e6;
    fSpriteRotateMatrix = rotate(angle);

    //fSpriteScale = spriteScale;

    fSpriteColor = spriteColor;
}
