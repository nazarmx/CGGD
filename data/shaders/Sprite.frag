/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#version 330 core
in mat2 fSpriteRotateMatrix;
//in vec2 fSpriteScale;
in vec4 fSpriteColor;
uniform sampler2D spriteTexture;
out vec4 outColor;

void main()
{
    const vec2 offset = vec2(0.5, 0.5);
    vec2 pos = gl_PointCoord - offset;
    //vec2 pos = gl_PointCoord*fSpriteScale - offset;
    pos.y = -pos.y;
    pos = fSpriteRotateMatrix * pos;
    pos.y = -pos.y;
    pos += offset;

    outColor = texture(spriteTexture, pos) * fSpriteColor;
}
