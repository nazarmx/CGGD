<#
   Copyright 2016 Nazar Mishturak
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
#>
Set-StrictMode -Version Latest
$DebugPreference = "Continue"

function Exec
{
    [CmdletBinding()]
    param (
        [Parameter(Position=0, Mandatory=1)]
        [string]$Command,
        [Parameter(Position=1, Mandatory=0)]
        [string]$ErrorMessage = "Execution of command failed.`n$Command"
    )
    Invoke-Expression ( $Command -replace '\s+',' ' )
    if ($LastExitCode -ne 0) {
        throw "Exec: $ErrorMessage"
    }
}

Push-Location "$Env:APPVEYOR_BUILD_FOLDER"
Exec "git submodule update --init"
if(!$Env:CONFIGURATION) {
    $Env:CONFIGURATION = "MinSizeRel"
}
Write-Debug "Installing dependencies for $Env:type-$Env:arch-$Env:CONFIGURATION..."

if ($Env:type -eq "mingw") {
    $Env:MSYSTEM = "MSYS"
    Exec "C:\msys64\usr\bin\pacman --noconfirm --sync --refresh --refresh --sysupgrade --sysupgrade"
    Exec "C:\msys64\usr\bin\pacman --noconfirm --sync --refresh --refresh --sysupgrade --sysupgrade"

    if($Env:arch -eq "win64") {
        $arch = "x86_64"
    }
    elseif($Env:arch -eq "win32") {
        $arch = "i686"
    }
    else {
        exit 1
    }
    Exec @"
    C:\msys64\usr\bin\pacman -S --needed --noconfirm mingw-w64-$arch-gcc
                                                     mingw-w64-$arch-cmake
                                                     mingw-w64-$arch-ninja
                                                     mingw-w64-$arch-glfw
                                                     mingw-w64-$arch-glbinding
"@
}
elseif($Env:type -eq "msvc") {
    if($Env:arch -eq "win64") {
        $gen = "Visual Studio 14 2015 Win64"
    }
    elseif($Env:arch -eq "win32") {
        $gen = "Visual Studio 14 2015"
    }

    # Compile GLFW
    Set-Location "$Env:APPVEYOR_BUILD_FOLDER"
    $glfwSourceDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "external", "glfw")
    Write-Debug "GLFW source dir: $glfwSourceDir"
    $glfwBuildDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "build-$Env:type-$Env:arch-$Env:CONFIGURATION-glfw")
    Write-Debug "GLFW build dir: $glfwBuildDir"
    $glfwInstallDir = [IO.Path]::Combine("$glfwBuildDir", "install")
    Write-Debug "GLFW install dir: $glfwInstallDir"

    # Check for existing build
    if(Test-Path -PathType Container "$glfwInstallDir") {
        Write-Debug "Reusing cached GLFW build: $glfwInstallDir"
    }
    else {
        Write-Debug "Building GLFW in: $glfwBuildDir"
        New-Item $glfwInstallDir -ItemType Directory | Out-Null
        Set-Location $glfwBuildDir
        Exec @"
        cmake -G "$gen"
              -DCMAKE_INSTALL_PREFIX=$glfwInstallDir
              -DGLFW_BUILD_EXAMPLES=OFF
              -DGLFW_BUILD_TESTS=OFF
              -DGLFW_BUILD_DOCS=OFF
              $glfwSourceDir
"@
        Exec "cmake --build . --target install --config $Env:CONFIGURATION"
    }
    
    # Compile glbinding
    Set-Location "$Env:APPVEYOR_BUILD_FOLDER"
    $glbindingSourceDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "external", "glbinding")
    Write-Debug "glbinding source dir: $glbindingSourceDir"
    $glbindingBuildDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "build-$Env:type-$Env:arch-$Env:CONFIGURATION-glbinding")
    Write-Debug "glbinding build dir: $glbindingBuildDir"
    $glbindingInstallDir = [IO.Path]::Combine("$glbindingBuildDir", "install")
    Write-Debug "glbinding install dir: $glbindingInstallDir"

    if(Test-Path -PathType Container "$glbindingInstallDir") {
        Write-Debug "Reusing cached glbinding build: $glbindingInstallDir"
    }
    else {
        Write-Debug "Building glbinding in: $glbindingInstallDir"
        New-Item $glbindingInstallDir -ItemType Directory -Force | Out-Null
        Set-Location $glbindingBuildDir
        Exec @"
        cmake -G "$gen"
              -DCMAKE_INSTALL_PREFIX=$glbindingInstallDir
              -DBUILD_SHARED_LIBS=OFF
              -DOPTION_BUILD_TESTS=OFF
              -DOPTION_BUILD_GPU_TESTS=OFF
              $glbindingSourceDir
"@
        Exec "cmake --build . --target install --config $Env:CONFIGURATION"
    }
}
else {
    Pop-Location
    exit 1
}
Pop-Location
