<#
   Copyright 2016 Nazar Mishturak
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
#>
Set-StrictMode -Version Latest
$DebugPreference = "Continue"

function Exec
{
    [CmdletBinding()]
    param (
        [Parameter(Position=0, Mandatory=1)]
        [string]$Command,
        [Parameter(Position=1, Mandatory=0)]
        [string]$ErrorMessage = "Execution of command failed.`n$Command"
    )
    Invoke-Expression ( $Command -replace '\s+',' ' )
    if ( ($LastExitCode -ne 0) -and [string]::IsNullOrEmpty($LastExitCode) ) {
        throw "Exec: $ErrorMessage"
    }
}

Push-Location "$Env:APPVEYOR_BUILD_FOLDER"

if(!$Env:CONFIGURATION) {
    $Env:CONFIGURATION = "MinSizeRel"
}
Write-Debug "Building project for $Env:type-$Env:arch-$Env:CONFIGURATION..."
if ($Env:type -eq "mingw") {
    if($Env:arch -eq "win64") {
        $Env:MSYSTEM = "MINGW64"
    }
    elseif($Env:arch -eq "win32") {
        $Env:MSYSTEM = "MINGW32"
    }
    $bashScriptFile = [System.IO.Path]::GetTempFileName()
    Set-Content $bashScriptFile @'
set -e 
set -o pipefail
cd $APPVEYOR_BUILD_FOLDER
mkdir build-mingw-$arch-$CONFIGURATION && cd build-mingw-$arch-$CONFIGURATION
$MINGW_MOUNT_POINT/bin/cmake.exe \
    -GNinja \
    -DCMAKE_BUILD_TYPE=$CONFIGURATION \
    -DCMAKE_C_COMPILER=$MINGW_MOUNT_POINT/bin/gcc.exe \
    -DCMAKE_CXX_COMPILER=$MINGW_MOUNT_POINT/bin/g++.exe \
    ..
$MINGW_MOUNT_POINT/bin/cmake.exe --build . --target package
'@
    Exec "C:\msys64\usr\bin\bash --login $bashScriptFile"
	Remove-Item -Force $bashScriptFile
}
elseif($Env:type -eq "msvc") {
    if($Env:arch -eq "win64") {
        $gen = "Visual Studio 14 2015 Win64"
    }
    elseif($Env:arch -eq "win32") {
        $gen = "Visual Studio 14 2015"
    }

    $glfwBuildDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "build-$Env:type-$Env:arch-$Env:CONFIGURATION-glfw")
    $glfwInstallDir = [IO.Path]::Combine("$glfwBuildDir", "install")

    $glbindingBuildDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "build-$Env:type-$Env:arch-$Env:CONFIGURATION-glbinding")
    $glbindingInstallDir = [IO.Path]::Combine("$glbindingBuildDir", "install")

    $projectBuildDir = [IO.Path]::Combine("$Env:APPVEYOR_BUILD_FOLDER", "build-$Env:type-$Env:arch-$Env:CONFIGURATION")

    New-Item $projectBuildDir -ItemType Directory -Force | Out-Null
    Set-Location "$projectBuildDir"

    Exec @"
    cmake -G "$gen"
          -DCMAKE_PREFIX_PATH=$glbindingInstallDir``;$glfwInstallDir
          ..
"@
    Exec "cmake --build . --target package --config $Env:CONFIGURATION"
}
else {
    Pop-Location
    exit 1
}
Pop-Location
