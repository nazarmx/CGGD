/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/types.h>
#include <iosfwd>
#include <string>

namespace CGGD {
namespace GL {

class Shader
{
public:
    Shader(gl33core::GLenum type);
    Shader(const Shader&) = delete;
    Shader& operator=(Shader&& other);
    Shader(Shader && other) noexcept;
    ~Shader();

    void setSource(const std::string& src);
    void setSourceFile(const std::string& filename);
    void setSourceFile(std::ifstream& file);
    bool compile();
    void destroy();

    std::string infoLog() const;
    bool isCompileOk() const;
    bool isDeleteFlagged() const;
    gl33core::GLenum type() const;
    gl33core::GLuint id() const;
private:
    gl33core::GLuint mID = 0;
    bool wasDestroyed = false;
};

} // namespace GL
} // namespace CGGD
