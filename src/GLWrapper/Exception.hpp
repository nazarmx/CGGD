/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <stdexcept>
#include <string>
#include <glbinding/FunctionCall.h>

namespace CGGD {
namespace GL {
class Exception : public std::runtime_error
{
public:
    Exception(const glbinding::FunctionCall& functionCall);
    Exception(const char *msg);
    Exception(const std::string& msg);
};
} // namespace GL
} // namespace CGGD
