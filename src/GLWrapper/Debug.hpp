/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/types.h>
#include <functional>
#include <string>
namespace CGGD {
namespace GL {
namespace Debug {
using Callback = std::function<void(gl33core::GLenum, gl33core::GLenum, gl33core::GLuint, gl33core::GLenum, const std::string& msg, const void *)>;
void setCallback(const Callback& f);
void resetCallback();

} // namespace Debug
} // namespace GL
} // namespace CGGD
