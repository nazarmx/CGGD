/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "ShaderProgram.hpp"
#include "Shader.hpp"
#include <glbinding/gl33core/gl.h>
#include <utility>
#include <cassert>
using namespace gl33core;
namespace CGGD {
namespace GL {

ShaderProgram::ShaderProgram() :
    mID{ glCreateProgram() }
{

}

ShaderProgram::ShaderProgram(ShaderProgram&& other) noexcept
{
    std::swap(other.mID, mID);
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram&& other)
{
    if(this != &other)
    {
        destroy();
        std::swap(other.mID, mID);
    }
    return *this;
}

ShaderProgram::~ShaderProgram()
{
    destroy();
}

bool ShaderProgram::isLinkOk() const
{
    assert(mID != 0);
    GLboolean ret = GL_FALSE;
    glGetProgramiv(mID, GL_LINK_STATUS, &ret);
    return (ret == GL_TRUE);
}

bool ShaderProgram::isValidationOk() const
{
    assert(mID != 0);
    GLboolean ret = GL_FALSE;
    glGetProgramiv(mID, GL_VALIDATE_STATUS, &ret);
    return (ret == GL_TRUE);
}

bool ShaderProgram::isDeleteFlagged() const
{
    assert(mID != 0);
    GLboolean ret = GL_FALSE;
    glGetProgramiv(mID, GL_DELETE_STATUS, &ret);
    return (ret == GL_TRUE);
}

gl33core::GLint ShaderProgram::uniformLocation(const std::string& name)
{
    assert(mID != 0);
    return glGetUniformLocation(mID, name.c_str());
}

gl33core::GLint ShaderProgram::attributeLocation(const std::string& name)
{
    assert(mID != 0);
    return glGetAttribLocation(mID, name.c_str());
}

std::string ShaderProgram::infoLog() const
{
    assert(mID != 0);
    GLint logMaxLength;
    glGetProgramiv(mID, GL_INFO_LOG_LENGTH, &logMaxLength);
    std::string ret(logMaxLength, '\0');

    GLsizei length;
    glGetProgramInfoLog(mID, static_cast<GLsizei>(ret.size()), &length, &ret[0]);
    ret.resize(length);
    return ret;
}

gl33core::GLuint ShaderProgram::currentlyUsedID()
{
     GLint ret = 0;
     glGetIntegerv(GL_CURRENT_PROGRAM, &ret);
     return static_cast<GLuint>(ret);
}

bool ShaderProgram::isUsed() const
{
    assert(mID != 0);
    return (currentlyUsedID() == mID);
}

gl33core::GLuint ShaderProgram::id() const
{
    return mID;
}

void ShaderProgram::use() const
{
    assert(mID != 0);
    glUseProgram(mID);
}

void ShaderProgram::unuse()
{
    glUseProgram(0);
}

void ShaderProgram::destroy()
{
    glDeleteProgram(mID);
    mID = 0;
}

void ShaderProgram::attachShader(const Shader& shader)
{
    assert(mID != 0);
    glAttachShader(mID, shader.id());
}

void ShaderProgram::detachShader(const Shader& shader)
{
    assert(mID != 0);
    glDetachShader(mID, shader.id());
}

void ShaderProgram::link()
{
    assert(mID != 0);
    glLinkProgram(mID);
}

void ShaderProgram::validate()
{
    assert(mID != 0);
    glValidateProgram(mID);
}

ShaderProgram::Binder::Binder(const ShaderProgram& prog) :
    mProgram{prog}
{
    mProgram.use();
}

ShaderProgram::Binder::~Binder()
{
    mProgram.unuse();
}


} // namespace GL
} // namespace CGGD
