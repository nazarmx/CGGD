/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Buffer.hpp"
#include <utility>
#include <cassert>

using namespace gl33core;
namespace CGGD {
namespace GL {

Buffer::Buffer()
{
    glGenBuffers(1, &mID);
}

Buffer::Buffer(const Buffer& other)
{    
    glGenBuffers(1, &mID);
    std::size_t otherSize = other.sizeBytes();
    setData<char>(nullptr, otherSize, GL_COPY_WRITE_BUFFER);
    other.copyTo(*this);
}

Buffer::Buffer(Buffer&& other) noexcept
{
    std::swap(other.mID, mID);
}

Buffer& Buffer::operator=(Buffer && other)
{
    if(this != &other)
    {
        destroy();
        std::swap(other.mID, mID);
    }
    return *this;
}

Buffer& Buffer::operator=(const Buffer& other)
{
    if(this == &other)
        return *this;

    std::size_t otherSize = other.sizeBytes();
    setData<char>(nullptr, otherSize, GL_COPY_WRITE_BUFFER);
    other.copyTo(*this);

    return *this;
}

Buffer::~Buffer()
{
    destroy();
}

void Buffer::copyTo(Buffer& target, std::size_t bytesSize, std::size_t readOffset, std::size_t writeOffset) const
{
    assert(this->mID != 0);
    assert(target.mID != 0);

    Binder thisBinder(*this, GL_COPY_READ_BUFFER);
    Binder targetBinder(target, GL_COPY_WRITE_BUFFER);

    // if sizeIs zero copy all
    bytesSize = (bytesSize == 0) ? sizeBytes(GL_COPY_READ_BUFFER) :
                                   bytesSize;

    glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, static_cast<GLintptr>(readOffset), static_cast<GLintptr>(writeOffset), static_cast<GLsizeiptr>(bytesSize));
}

gl33core::GLuint Buffer::currentlyBoundID(gl33core::GLenum tgt)
{
    gl33core::GLenum arg = GL_INVALID_ENUM;
    switch(tgt)
    {
    case GL_ARRAY_BUFFER:
        arg = GL_ARRAY_BUFFER_BINDING;
        break;
    case GL_ELEMENT_ARRAY_BUFFER:
        arg = GL_ELEMENT_ARRAY_BUFFER_BINDING;
        break;
    case GL_COPY_READ_BUFFER:
        arg = GL_COPY_READ_BUFFER_BINDING;
        break;
    case GL_COPY_WRITE_BUFFER:
        arg = GL_COPY_WRITE_BUFFER_BINDING;
        break;
    default:
        return 0;
    }

    GLint ret = 0;
    glGetIntegerv(arg, &ret);
    return static_cast<GLuint>(ret);
}

bool Buffer::isBound(gl33core::GLenum target) const
{
    assert(mID != 0);
    return ( currentlyBoundID(target) == mID );
}


std::size_t Buffer::sizeBytes(gl33core::GLenum target) const
{
    GLint64 ret = 0;
    using FPtr = void(*)(GLenum, GLenum, GLint64*);
    FPtr ptr = &glGetBufferParameteri64v;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_SIZE, &ret));
    return static_cast<std::size_t>(ret);
}

gl33core::GLenum Buffer::usage(gl33core::GLenum target) const
{
    GLenum ret = GL_INVALID_ENUM;
    using FPtr = void(*)(GLenum, GLenum, GLenum*);
    FPtr ptr = &glGetBufferParameteriv;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_USAGE, &ret));
    return ret;
}

void* Buffer::map(gl33core::GLenum access, gl33core::GLenum target)
{
    using FPtr = void*(*)(GLenum, GLenum);
    FPtr ptr = &glMapBuffer;
    return doScoped(target, std::bind(ptr, std::placeholders::_1, access));
}


void *Buffer::mapRange(std::size_t offset, std::size_t length, gl33core::BufferAccessMask access, gl33core::GLenum target)
{
    using FPtr = void*(*)(GLenum, GLintptr, GLsizeiptr, gl33core::BufferAccessMask);
    FPtr ptr = &glMapBufferRange;
    return doScoped(target, std::bind(ptr, std::placeholders::_1, static_cast<GLintptr>(offset),  static_cast<GLsizeiptr>(length), access));
}


void Buffer::flushMappedRange(std::size_t offset, std::size_t length, gl33core::GLenum target)
{
    using FPtr = void(*)(GLenum, GLintptr, GLsizeiptr);
    FPtr ptr = &glFlushMappedBufferRange;
    callScoped(target, std::bind(ptr, std::placeholders::_1, static_cast<GLintptr>(offset),  static_cast<GLsizeiptr>(length)));
}

gl33core::GLenum Buffer::accessMode(gl33core::GLenum target) const
{
    GLenum ret = GL_INVALID_ENUM;
    using FPtr = void(*)(GLenum, GLenum, GLenum*);
    FPtr ptr = &glGetBufferParameteriv;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_ACCESS, &ret));
    return ret;
}

gl33core::BufferAccessMask Buffer::accessMask(gl33core::GLenum target) const
{
    GLint ret = 0;
    using FPtr = void(*)(GLenum, GLenum, GLint*);
    FPtr ptr = &glGetBufferParameteriv;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_ACCESS_FLAGS, &ret));
    return static_cast<BufferAccessMask>(static_cast<GLuint>(ret));
}

bool Buffer::isMapped(gl33core::GLenum target) const
{
    GLboolean ret = GL_FALSE;
    using FPtr = void(*)(GLenum, GLenum, GLboolean*);
    FPtr ptr = &glGetBufferParameteriv;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_MAPPED, &ret));
    return (ret == GL_TRUE);
}

std::size_t Buffer::mapLength(gl33core::GLenum target) const
{
    GLint64 ret = 0;
    using FPtr = void(*)(GLenum, GLenum, GLint64*);
    FPtr ptr = &glGetBufferParameteri64v;
    callScoped(target, std::bind(ptr, std::placeholders::_1,  GL_BUFFER_MAP_LENGTH , &ret));
    return static_cast<std::size_t>(ret);
}

std::size_t Buffer::mapOffset(gl33core::GLenum target) const
{
    GLint64 ret = 0;
    using FPtr = void(*)(GLenum, GLenum, GLint64*);
    FPtr ptr = &glGetBufferParameteri64v;
    callScoped(target, std::bind(ptr, std::placeholders::_1,  GL_BUFFER_MAP_OFFSET , &ret));
    return static_cast<std::size_t>(ret);
}

void* Buffer::getPointer(gl33core::GLenum target) const
{
    GLvoid* ret = nullptr;
    using FPtr = void(*)(GLenum, GLenum, GLvoid**);
    FPtr ptr = &glGetBufferPointerv;
    callScoped(target, std::bind(ptr, std::placeholders::_1, GL_BUFFER_MAP_POINTER, &ret));
    return ret;
}

bool Buffer::unmap(gl33core::GLenum target)
{
    using FPtr = GLboolean(*)(GLenum);
    FPtr ptr = &glUnmapBuffer;
    return (doScoped(target, std::bind(ptr, std::placeholders::_1)) == GL_TRUE);
}

void Buffer::bind(gl33core::GLenum target) const
{
    assert(mID != 0);
    glBindBuffer(target, mID);
}

void Buffer::unbind(gl33core::GLenum target)
{
    glBindBuffer(target, 0);
}

void Buffer::destroy()
{
    glDeleteBuffers(1, &mID);
    mID = 0;
}

gl33core::GLuint Buffer::id() const
{
    return mID;
}

Buffer::Binder::Binder(const Buffer& buffer, gl33core::GLenum target) :
    mBufferRef{buffer},
    mTarget{target}
{
    mBufferRef.bind(mTarget);
}

Buffer::Binder::~Binder()
{
    mBufferRef.unbind(mTarget);
}

} // namespace GL
} // namespace CGGD
