/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/gl.h>
#include <vector>
#include <array>
#include <cassert>
#include <functional>
namespace CGGD {
namespace GL {
class Buffer
{
public:
    class Binder {
    public:
        Binder(const Buffer& buffer, gl33core::GLenum target = gl33core::GL_ARRAY_BUFFER);
        Binder(const Binder&) = delete;
        Binder& operator=(const Binder&) = delete;
        Binder(Binder&&) = delete;
        Binder& operator=(Binder&&) = delete;

        ~Binder();
    private:
        const Buffer& mBufferRef;
        const gl33core::GLenum mTarget;
    };

    Buffer();
    Buffer(const Buffer& other);
    Buffer(Buffer && other) noexcept;

    Buffer& operator=(Buffer && other);
    Buffer& operator=(const Buffer & other);
    ~Buffer();

    void copyTo(Buffer& target, std::size_t bytesSize = 0, std::size_t readOffset = 0, std::size_t writeOffset = 0) const;
    template<typename T> void setData(const std::vector<T> & data, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0, gl33core::GLenum usage = gl33core::GL_STATIC_DRAW)
    {
        size = (size == 0) ?
                    data.size() :
                    size;
        assert(size <= data.size());
        callScoped(target, std::bind(&gl33core::glBufferData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLsizeiptr>(size * sizeof(T)),
                                                             static_cast<const void*>(data.data()),
                                                             usage)
                   );
    }

    template<typename T, std::size_t N> void setData(const std::array<T, N> & data, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0, gl33core::GLenum usage = gl33core::GL_STATIC_DRAW)
    {
        size = (size == 0) ?
                    N :
                    size;
        assert(size <= N);
        callScoped(target, std::bind(&gl33core::glBufferData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLsizeiptr>(size * sizeof(T)),
                                                             static_cast<const void*>(data.data()),
                                                             usage)
                   );
    }

    template<typename T, std::size_t N> void setData(const T (&data)[N], gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0, gl33core::GLenum usage = gl33core::GL_STATIC_DRAW)
    {
        size = (size == 0) ?
                   N :
                   size;
        assert(size <= N);
        callScoped(target, std::bind(&gl33core::glBufferData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLsizeiptr>(size * sizeof(T)),
                                                             static_cast<const void*>(data),
                                                             usage)
                   );
    }
    template<typename T> void setData(const T *data, std::size_t size, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, gl33core::GLenum usage = gl33core::GL_STATIC_DRAW)
    {
        callScoped(target, std::bind(&gl33core::glBufferData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLsizeiptr>(size * sizeof(T)),
                                                             static_cast<const void*>(data),
                                                             usage)
                   );
    }
    void setDataRaw(const void *data, std::size_t sizeBytes, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, gl33core::GLenum usage = gl33core::GL_STATIC_DRAW)
    {
        callScoped(target, std::bind(&gl33core::glBufferData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLsizeiptr>(sizeBytes),
                                                             data,
                                                             usage)
                   );
    }

    template<typename T> void updateData(std::size_t offset, const std::vector<T> & data, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0)
    {
        size = (size == 0) ?
                   data.size() :
                   size;
        assert(size <= data.size());
        callScoped(target, std::bind(&gl33core::glBufferSubData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLintptr>(offset*sizeof(T)),
                                                             static_cast<gl33core::GLsizeiptr>(size*sizeof(T)),
                                                             static_cast<const void*>(data.data()))
                   );
    }

    template<typename T, std::size_t N> void updateData(std::size_t offset, const std::array<T, N> & data, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0)
    {
        size = (size == 0) ?
                   N :
                   size;
        assert(size <= N);
        callScoped(target, std::bind(&gl33core::glBufferSubData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLintptr>(offset*sizeof(T)),
                                                             static_cast<gl33core::GLsizeiptr>(size*sizeof(T)),
                                                             static_cast<const void*>(data.data()))
                   );
    }

    template<typename T, std::size_t N> void updateData(std::size_t offset, const T (&data)[N], gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER, std::size_t size = 0)
    {
        size = (size == 0) ?
                   N :
                   size;
        assert(size <= N);
        callScoped(target, std::bind(&gl33core::glBufferSubData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLintptr>(offset*sizeof(T)),
                                                             static_cast<gl33core::GLsizeiptr>(size*sizeof(T)),
                                                             static_cast<const void*>(data))
                   );
    }
    template<typename T> void updateData(std::size_t offset, const T *data, std::size_t size, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER)
    {
        callScoped(target, std::bind(&gl33core::glBufferSubData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLintptr>(offset*sizeof(T)),
                                                             static_cast<gl33core::GLsizeiptr>(size*sizeof(T)),
                                                             static_cast<const void*>(data))
                   );
    }
    void updateDataRaw(std::size_t offsetBytes, const void *data, std::size_t sizeBytes, gl33core::GLenum target = gl33core::GL_COPY_WRITE_BUFFER)
    {
        callScoped(target, std::bind(&gl33core::glBufferSubData,
                                                             std::placeholders::_1,
                                                             static_cast<gl33core::GLintptr>(offsetBytes),
                                                             static_cast<gl33core::GLsizeiptr>(sizeBytes),
                                                             data)
                   );
    }

    static gl33core::GLuint currentlyBoundID(gl33core::GLenum tgt = gl33core::GL_ARRAY_BUFFER);

    bool isBound(gl33core::GLenum target = gl33core::GL_ARRAY_BUFFER) const;
    std::size_t sizeBytes(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    gl33core::GLenum usage(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;

    void* map(gl33core::GLenum access = gl33core::GL_READ_WRITE, gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER);

    void* mapRange(std::size_t offset, std::size_t length,
                   gl33core::BufferAccessMask access = (gl33core::GL_MAP_READ_BIT | gl33core::GL_MAP_WRITE_BIT),
                   gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER
                  );

    void flushMappedRange(std::size_t offset, std::size_t length, gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER);
    gl33core::GLenum accessMode(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    gl33core::BufferAccessMask accessMask(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    bool isMapped(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    std::size_t mapLength(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    std::size_t mapOffset(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    void* getPointer(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER) const;
    bool unmap(gl33core::GLenum target = gl33core::GL_COPY_READ_BUFFER);

    void bind(gl33core::GLenum target = gl33core::GL_ARRAY_BUFFER) const;
    static void unbind(gl33core::GLenum target = gl33core::GL_ARRAY_BUFFER);

    void destroy();

    gl33core::GLuint id() const;

private:
    template<typename F> auto doScoped(gl33core::GLenum target, const F& func) const -> decltype(func(target))
    {
        assert(mID != 0);

        if( (target != gl33core::GL_COPY_READ_BUFFER) && (target != gl33core::GL_COPY_WRITE_BUFFER) )
            return func(target); // assume we are bound and need no scoping

        const gl33core::GLuint prevID = currentlyBoundID(target);
        if(prevID != mID) // we are not bound
            bind(target);

        auto ret = func(target);

        if(prevID != mID) // restore prev
            gl33core::glBindBuffer(target, prevID);
        return ret;
    }
    template<typename F> auto callScoped(gl33core::GLenum target, const F& func) const -> void
    {
        assert(mID != 0);

        if( (target != gl33core::GL_COPY_READ_BUFFER) && (target != gl33core::GL_COPY_WRITE_BUFFER) )
        {
            func(target); // assume we are bound and need no scoping
            return;
        }

        const gl33core::GLuint prevID = currentlyBoundID(target);
        if(prevID != mID) // we are not bound
            bind(target);

        func(target);

        if(prevID != mID) // restore prev
            gl33core::glBindBuffer(target, prevID);
    }

    gl33core::GLuint mID = 0;
};




} // namespace GL
} // namespace CGGD
