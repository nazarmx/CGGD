/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Texture.hpp"


#if defined(_MSC_VER)
#pragma warning(push, 0)
#endif
#include <gli/texture.hpp>
#include <gli/load.hpp>
#include <gli/gl.hpp>
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#include <glbinding/gl33ext/gl.h>
#include <glbinding/ContextInfo.h>
#include <utility>
#include <cassert>
using namespace gl33core;

namespace CGGD {
namespace GL {

Texture::Texture()
{
    glGenTextures(1, &mID);
}

Texture& Texture::operator=(Texture&& other)
{
    if(&other != this)
    {
        destroy();
        std::swap(other.mID, mID);
    }
    return *this;
}

Texture::Texture(Texture&& other) noexcept
{
    std::swap(other.mID, mID);
}

Texture::~Texture()
{
    destroy();
}

void Texture::setActiveUnit(gl33core::GLuint unit)
{
    const GLuint arg = static_cast<GLuint>(GL_TEXTURE0) + unit;
    glActiveTexture(static_cast<GLenum>(arg));
}

void Texture::setActiveUnit(gl33core::GLenum unit)
{
    const GLuint arg = static_cast<GLuint>(GL_TEXTURE0) + static_cast<GLuint>(unit);
    glActiveTexture(static_cast<GLenum>(arg));
}

bool Texture::loadFromFile(const std::string& fileName)
{
    enum class TexStorageSupported {
        DontKnow,
        Yes,
        No
    };
    static TexStorageSupported supportedFlag = TexStorageSupported::DontKnow;
    if(supportedFlag == TexStorageSupported::DontKnow)
    {
        const auto exts = glbinding::ContextInfo::extensions();
        const bool haveTexStorage = (exts.find(GLextension::GL_ARB_texture_storage) != exts.end());
        if(haveTexStorage)
            supportedFlag = TexStorageSupported::Yes;
        else
            supportedFlag = TexStorageSupported::No;
    }

    switch(supportedFlag)
    {
    case TexStorageSupported::Yes:
        return loadTexStorageInternal(fileName);
    case TexStorageSupported::No:
        return loadTexInternal(fileName);
    default:
        assert(supportedFlag != TexStorageSupported::DontKnow);
        return false;
    }
}

gl33core::GLuint Texture::activeUnit()
{
    GLint ret = 0;
    glGetIntegerv(GL_ACTIVE_TEXTURE, &ret);

    return static_cast<GLuint>(ret);
}

gl33core::GLuint Texture::id() const
{
    return mID;
}

void Texture::bind(gl33core::GLenum target) const
{
    assert(mID != 0);
    glBindTexture(target, mID);
}

void Texture::unbind(gl33core::GLenum target)
{
    glBindTexture(target, 0);
}

gl33core::GLuint Texture::currentlyBoundID(gl33core::GLenum target)
{
    GLenum arg = GL_INVALID_ENUM;
    switch(target)
    {
    case GL_TEXTURE_1D:
        arg = GL_TEXTURE_BINDING_1D;
        break;
    case GL_TEXTURE_1D_ARRAY:
        arg = GL_TEXTURE_BINDING_1D_ARRAY;
        break;
    case GL_TEXTURE_2D:
        arg = GL_TEXTURE_BINDING_2D;
        break;
    case GL_TEXTURE_2D_ARRAY:
        arg = GL_TEXTURE_BINDING_2D_ARRAY;
        break;
    case GL_TEXTURE_2D_MULTISAMPLE:
        arg = GL_TEXTURE_BINDING_2D_MULTISAMPLE;
        break;
    case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
        arg = GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
        break;
    case GL_TEXTURE_3D:
        arg = GL_TEXTURE_BINDING_3D;
        break;
    case GL_TEXTURE_BUFFER:
        arg = GL_TEXTURE_BINDING_BUFFER;
        break;
    case GL_TEXTURE_CUBE_MAP:
        arg = GL_TEXTURE_BINDING_CUBE_MAP;
        break;
    case GL_TEXTURE_RECTANGLE:
        arg = GL_TEXTURE_BINDING_RECTANGLE;
        break;
    default:
        return 0;
    }
    GLint ret = 0;
    glGetIntegerv(arg, &ret);
    return static_cast<GLuint>(ret);
}

void Texture::destroy()
{
    glDeleteTextures(1, &mID);
    mID = 0;
}

bool Texture::loadTexStorageInternal(const std::string& fileName)
{
    const gli::texture Texture = gli::load(fileName);
    if(Texture.empty())
        return false;

    const gli::gl GL(gli::gl::PROFILE_GL33);
    const gli::gl::format Format = GL.translate(Texture.format(), Texture.swizzles());
    const GLenum Target = static_cast<GLenum>(GL.translate(Texture.target()));

    Binder texBinder(*this, Target);

    glTexParameteri(Target, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(Target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(Texture.levels() - 1));
    glTexParameteri(Target, GL_TEXTURE_SWIZZLE_R, Format.Swizzles[0]);
    glTexParameteri(Target, GL_TEXTURE_SWIZZLE_G, Format.Swizzles[1]);
    glTexParameteri(Target, GL_TEXTURE_SWIZZLE_B, Format.Swizzles[2]);
    glTexParameteri(Target, GL_TEXTURE_SWIZZLE_A, Format.Swizzles[3]);
    if(Texture.levels() >= 1)
        glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else
        glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(Target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(Target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(Target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(Target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    const glm::tvec3<GLsizei> Extent(Texture.extent());
    const GLsizei FaceTotal = static_cast<GLsizei>(Texture.layers() * Texture.faces());

    switch(Texture.target())
    {
    case gli::TARGET_1D:
        gl33ext::glTexStorage1D(
                    Target, static_cast<GLint>(Texture.levels()), static_cast<GLenum>(Format.Internal), Extent.x);
        break;
    case gli::TARGET_1D_ARRAY:
    case gli::TARGET_2D:
    case gli::TARGET_CUBE:
        gl33ext::glTexStorage2D(
                    Target, static_cast<GLint>(Texture.levels()), static_cast<GLenum>(Format.Internal),
                    Extent.x, Texture.target() == gli::TARGET_2D ? Extent.y : FaceTotal);
        break;
    case gli::TARGET_2D_ARRAY:
    case gli::TARGET_3D:
    case gli::TARGET_CUBE_ARRAY:
        gl33ext::glTexStorage3D(
                    Target, static_cast<GLint>(Texture.levels()), static_cast<GLenum>(Format.Internal),
                    Extent.x, Extent.y,
                    Texture.target() == gli::TARGET_3D ? Extent.z : FaceTotal);
        break;
    default:
        return false;
    }

    for(std::size_t Layer = 0; Layer < Texture.layers(); ++Layer)
        for(std::size_t Face = 0; Face < Texture.faces(); ++Face)
            for(std::size_t Level = 0; Level < Texture.levels(); ++Level)
            {
                GLsizei const LayerGL = static_cast<GLsizei>(Layer);
                glm::tvec3<GLsizei> loopExtent(Texture.extent(Level));
                GLenum loopTarget = gli::is_target_cube(Texture.target())
                        ? static_cast<GLenum>(static_cast<std::size_t>(GL_TEXTURE_CUBE_MAP_POSITIVE_X) + Face)
                        : Target;

                switch(Texture.target())
                {
                case gli::TARGET_1D:
                    if(gli::is_compressed(Texture.format()))
                        glCompressedTexSubImage1D(
                                    loopTarget, static_cast<GLint>(Level), 0, loopExtent.x,
                                    static_cast<GLenum>(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
                                    Texture.data(Layer, Face, Level));
                    else
                        glTexSubImage1D(
                                    loopTarget, static_cast<GLint>(Level), 0, loopExtent.x,
                                    static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                                    Texture.data(Layer, Face, Level));
                    break;
                case gli::TARGET_1D_ARRAY:
                case gli::TARGET_2D:
                case gli::TARGET_CUBE:
                    if(gli::is_compressed(Texture.format()))
                        glCompressedTexSubImage2D(
                                    loopTarget, static_cast<GLint>(Level),
                                    0, 0,
                                    loopExtent.x,
                                    Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : loopExtent.y,
                                    static_cast<GLenum>(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
                                    Texture.data(Layer, Face, Level));
                    else
                        glTexSubImage2D(
                                    loopTarget, static_cast<GLint>(Level),
                                    0, 0,
                                    loopExtent.x,
                                    Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : loopExtent.y,
                                    static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                                    Texture.data(Layer, Face, Level));
                    break;
                case gli::TARGET_2D_ARRAY:
                case gli::TARGET_3D:
                case gli::TARGET_CUBE_ARRAY:
                    if(gli::is_compressed(Texture.format()))
                        glCompressedTexSubImage3D(
                                    loopTarget, static_cast<GLint>(Level),
                                    0, 0, 0,
                                    loopExtent.x, loopExtent.y,
                                    Texture.target() == gli::TARGET_3D ? loopExtent.z : LayerGL,
                                    static_cast<GLenum>(Format.Internal), static_cast<GLsizei>(Texture.size(Level)),
                                    Texture.data(Layer, Face, Level));
                    else
                        glTexSubImage3D(
                                    loopTarget, static_cast<GLint>(Level),
                                    0, 0, 0,
                                    loopExtent.x, loopExtent.y,
                                    Texture.target() == gli::TARGET_3D ? loopExtent.z : LayerGL,
                                    static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                                    Texture.data(Layer, Face, Level));
                    break;
                default:
                    return false;
                }
            }
    return true;
}

bool Texture::loadTexInternal(const std::string& fileName)
{
      gli::texture Texture = gli::load(fileName);
      if(Texture.empty())
          return 0;

      const gli::gl GL(gli::gl::PROFILE_GL33);
      const gli::gl::format Format = GL.translate(Texture.format(), Texture.swizzles());
      GLenum Target = static_cast<GLenum>(GL.translate(Texture.target()));

      Binder texBinder(*this, Target);
      glTexParameteri(Target, GL_TEXTURE_BASE_LEVEL, 0);
      glTexParameteri(Target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(Texture.levels() - 1));
      glTexParameteri(Target, GL_TEXTURE_SWIZZLE_R, Format.Swizzles[0]);
      glTexParameteri(Target, GL_TEXTURE_SWIZZLE_G, Format.Swizzles[1]);
      glTexParameteri(Target, GL_TEXTURE_SWIZZLE_B, Format.Swizzles[2]);
      glTexParameteri(Target, GL_TEXTURE_SWIZZLE_A, Format.Swizzles[3]);
      if(Texture.levels() >= 1)
          glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      else
          glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(Target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(Target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(Target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(Target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

      //glm::tvec3<GLsizei> const Extent(Texture.extent());

      for(std::size_t Layer = 0; Layer < Texture.layers(); ++Layer)
      for(std::size_t Level = 0; Level < Texture.levels(); ++Level)
      for(std::size_t Face = 0; Face < Texture.faces(); ++Face)
      {
          GLsizei const LayerGL = static_cast<GLsizei>(Layer);
          glm::tvec3<GLsizei> loopExtent(Texture.extent(Level));
          Target = gli::is_target_cube(Texture.target())
              ? static_cast<GLenum>(static_cast<GLint>(GL_TEXTURE_CUBE_MAP_POSITIVE_X) + static_cast<GLint>(Face))
              : Target;

          switch(Texture.target())
          {
          case gli::TARGET_1D:
              if(gli::is_compressed(Texture.format()))
                  glCompressedTexImage1D(
                      Target,
                      static_cast<GLint>(Level),
                      static_cast<GLenum>(static_cast<GLenum>(Format.Internal)),
                      0, loopExtent.x,
                      static_cast<GLsizei>(Texture.size(Level)),
                      Texture.data(Layer, Face, Level));
              else
                  glTexImage1D(
                      Target, static_cast<GLint>(Level),
                      static_cast<GLenum>(Format.Internal),
                      loopExtent.x,
                      0,
                      static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                      Texture.data(Layer, Face, Level));
              break;
          case gli::TARGET_1D_ARRAY:
          case gli::TARGET_2D:
          case gli::TARGET_CUBE:
              if(gli::is_compressed(Texture.format()))
                  glCompressedTexImage2D(
                      Target, static_cast<GLint>(Level),
                      static_cast<GLenum>(Format.Internal),
                      loopExtent.x,
                      Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : loopExtent.y,
                      0,
                      static_cast<GLsizei>(Texture.size(Level)),
                      Texture.data(Layer, Face, Level));
              else
                  glTexImage2D(
                      Target, static_cast<GLint>(Level),
                      static_cast<GLenum>(Format.Internal),
                      loopExtent.x,
                      Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : loopExtent.y,
                      0,
                      static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                      Texture.data(Layer, Face, Level));
              break;
          case gli::TARGET_2D_ARRAY:
          case gli::TARGET_3D:
          case gli::TARGET_CUBE_ARRAY:
              if(gli::is_compressed(Texture.format()))
                  glCompressedTexImage3D(
                      Target, static_cast<GLint>(Level),
                      static_cast<GLenum>(Format.Internal),
                      loopExtent.x, loopExtent.y,
                      Texture.target() == gli::TARGET_3D ? loopExtent.z : LayerGL,
                      0,
                      static_cast<GLsizei>(Texture.size(Level)),
                      Texture.data(Layer, Face, Level));
              else
                  glTexImage3D(
                      Target, static_cast<GLint>(Level),
                      static_cast<GLenum>(Format.Internal),
                      loopExtent.x, loopExtent.y,
                      Texture.target() == gli::TARGET_3D ? loopExtent.z : LayerGL,
                      0,
                      static_cast<GLenum>(Format.External), static_cast<GLenum>(Format.Type),
                      Texture.data(Layer, Face, Level));
              break;
          default:
              return false;
          }
      }
      return true;
}

Texture::Binder::Binder(const Texture& tex, gl33core::GLenum target) :
    mTexture{tex},
    mTarget{target}
{
    mTexture.bind(mTarget);
}

Texture::Binder::~Binder()
{
    mTexture.unbind(mTarget);
}


} // namespace GL
} // namespace CGGD
