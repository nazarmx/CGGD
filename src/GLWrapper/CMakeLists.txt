#[[
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
]]#
list(APPEND SRC_LIST
    Shader.cpp
    Exception.cpp
    Texture.cpp
    Buffer.cpp
    VertexArrayObject.cpp
    ShaderProgram.cpp
    Debug.cpp
)

add_library(GLWrapper STATIC ${SRC_LIST})
target_link_libraries(GLWrapper PUBLIC glbinding::glbinding)
cotire(GLWrapper)
