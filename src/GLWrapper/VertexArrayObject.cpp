/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "VertexArrayObject.hpp"
#include <glbinding/gl33core/gl.h>
#include <utility>
#include <cassert>
using namespace gl33core;
namespace CGGD {
namespace GL {

VertexArrayObject::VertexArrayObject()
{
    glGenVertexArrays(1, &mID);
}

VertexArrayObject::VertexArrayObject(VertexArrayObject&& other) noexcept
{
    std::swap(other.mID, mID);
}

VertexArrayObject& VertexArrayObject::operator=(VertexArrayObject&& other)
{
    if(this != &other)
    {
        destroy();
        std::swap(other.mID, mID);
    }
    return *this;
}

VertexArrayObject::~VertexArrayObject()
{
    destroy();
}

gl33core::GLuint VertexArrayObject::currentlyBoundID()
{
    GLint ret = 0;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &ret);
    return static_cast<GLuint>(ret);
}

bool VertexArrayObject::isBound() const
{
    assert(mID != 0);
    return (currentlyBoundID() == mID);
}

void VertexArrayObject::bind() const
{
    assert(mID != 0);
    glBindVertexArray(mID);
}

void VertexArrayObject::unbind()
{
    glBindVertexArray(0);
}

void VertexArrayObject::destroy()
{
    glDeleteVertexArrays(1, &mID);
    mID = 0;
}

gl33core::GLuint VertexArrayObject::id() const
{
    return mID;
}

VertexArrayObject::Binder::Binder(const VertexArrayObject& object) :
    mVAORef{object}
{
    mVAORef.bind();
}

VertexArrayObject::Binder::~Binder()
{
    mVAORef.unbind();
}

} // namespace GL
} // namespace CGGD
