/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/types.h>

namespace CGGD {
namespace GL {
class VertexArrayObject
{
public:
    class Binder {
    public:
        Binder(const VertexArrayObject& object);
        Binder(const Binder&) = delete;
        Binder& operator=(const Binder&) = delete;
        Binder(Binder&&) = delete;
        Binder& operator=(Binder&&) = delete;

        ~Binder();
    private:
        const VertexArrayObject& mVAORef;
    };

    VertexArrayObject();
    VertexArrayObject(const VertexArrayObject&) = delete;
    VertexArrayObject(VertexArrayObject && other) noexcept;
    VertexArrayObject& operator=(VertexArrayObject && other);
    ~VertexArrayObject();

    static gl33core::GLuint currentlyBoundID();
    bool isBound() const;
    void bind() const;
    static void unbind();
    void destroy();

    gl33core::GLuint id() const;
private:
    gl33core::GLuint mID = 0;
};

} // namespace GL
} // namespace CGGD
