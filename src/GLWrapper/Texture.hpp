/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/gl.h>
#include <string>
namespace CGGD {
namespace GL {
class Texture
{
public:
    class Binder {
    public:
        Binder(const Texture& tex, gl33core::GLenum target = gl33core::GL_TEXTURE_2D);
        Binder(const Binder&) = delete;
        Binder& operator=(const Binder&) = delete;
        Binder(Binder&&) = delete;
        Binder& operator=(Binder&&) = delete;

        ~Binder();
    private:
        const Texture& mTexture;
        const gl33core::GLenum mTarget;
    };
    Texture();
    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = delete;
    Texture& operator=(Texture&& other);
    Texture(Texture && other) noexcept;

    ~Texture();
    static void setActiveUnit(gl33core::GLuint unit);
    static void setActiveUnit(gl33core::GLenum unit);
    bool loadFromFile(const std::string& fileName);
    static gl33core::GLuint activeUnit();
    gl33core::GLuint id() const;
    void bind(gl33core::GLenum target = gl33core::GL_TEXTURE_2D) const;
    static void unbind(gl33core::GLenum target = gl33core::GL_TEXTURE_2D);
    static gl33core::GLuint currentlyBoundID(gl33core::GLenum target = gl33core::GL_TEXTURE_2D);
    void destroy();
private:
    bool loadTexStorageInternal(const std::string& fileName);
    bool loadTexInternal(const std::string& fileName);

    gl33core::GLuint mID = 0;
};
} // namespace GL
} // namespace CGGD
