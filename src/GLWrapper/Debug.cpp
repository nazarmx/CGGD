/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Debug.hpp"
#include "Exception.hpp"
#include <glbinding/ContextInfo.h>
#include <glbinding/gl33core/gl.h>
#include <glbinding/gl33ext/gl.h>
using namespace gl33core;

namespace {
CGGD::GL::Debug::Callback currentCallback;
bool isInitialized = false;
void GL_APIENTRY internalDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *msg, const void *data)
{
    /*std::cerr << "###########********  Debug Callback Start ********###########\n"
        << "source=" << glbinding::Meta::getString(source) << '\n'
        << "type=" << glbinding::Meta::getString(type) << '\n'
        << "id=" << id << '\n'
        << "severity=" << glbinding::Meta::getString(severity) << '\n'
        << "length=" << length << '\n'
        << "msg=" << msg << '\n'
        << "data=" << data << '\n' <<
           "###########********  Debug Callback End ********###########\n";*/

    if(currentCallback)
    {
        currentCallback(source, type, id, severity, std::string(msg, length), data);
    }
}

} // anonymous namespace
namespace CGGD {
namespace GL {
namespace Debug {
void setCallback(const Callback& f)
{
    if(!isInitialized)
    {
        auto exts = glbinding::ContextInfo::extensions();
        if(exts.find(GLextension::GL_KHR_debug) != exts.end())
        {
            glEnable(gl33ext::GL_DEBUG_OUTPUT);
            glDisable(gl33ext::GL_DEBUG_OUTPUT_SYNCHRONOUS);
            gl33ext::glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
            gl33ext::glDebugMessageCallback(&internalDebugCallback, nullptr);
        }
        else if(exts.find(GLextension::GL_ARB_debug_output) != exts.end())
        {
            glDisable(gl33ext::GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
            gl33ext::glDebugMessageCallbackARB(&internalDebugCallback, nullptr);
            gl33ext::glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
        }
        else
            throw CGGD::GL::Exception("CGGD::GL::Debug::setCallback() debugging not supported!");
        isInitialized = true;
    }
    currentCallback = f;
}

void resetCallback()
{
    currentCallback = Callback{};
}


} // namespace Debug
} // namespace GL
} // namespace CGGD
