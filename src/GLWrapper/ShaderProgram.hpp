/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/types.h>
#include <string>

namespace CGGD {
namespace GL {
class Shader;

class ShaderProgram
{
public:
    class Binder {
    public:
        Binder(const ShaderProgram& prog);
        Binder(const Binder&) = delete;
        Binder& operator=(const Binder&) = delete;
        Binder(Binder&&) = delete;
        Binder& operator=(Binder&&) = delete;

        ~Binder();
    private:
        const ShaderProgram& mProgram;
    };

    ShaderProgram();
    ShaderProgram(const ShaderProgram&) = delete;
    ShaderProgram(ShaderProgram && other) noexcept;
    ShaderProgram& operator=(ShaderProgram && other);
    ~ShaderProgram();

    bool isLinkOk() const;
    bool isValidationOk() const;
    bool isDeleteFlagged() const;
    gl33core::GLint uniformLocation(const std::string& name);
    gl33core::GLint attributeLocation(const std::string& name);

    std::string infoLog() const;
    static gl33core::GLuint currentlyUsedID();
    bool isUsed() const;
    gl33core::GLuint id() const;
    void use() const;
    static void unuse();
    void destroy();

    void attachShader(const Shader& shader);
    void detachShader(const Shader& shader);

    void link();
    void validate();

private:
    gl33core::GLuint mID = 0;
};

} // namespace GL
} // namespace CGGD
