/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Shader.hpp"
#include <glbinding/gl33core/gl.h>
#include <utility>
#include <fstream>
#include <cassert>
#include <stdexcept>
using namespace gl33core;
namespace CGGD {
namespace GL {


Shader::Shader(gl33core::GLenum type) :
    mID{ glCreateShader(type) }
{

}

Shader& Shader::operator=(Shader&& other)
{
    if(this != &other)
    {
        destroy();
        std::swap(other.mID, mID);
        std::swap(other.wasDestroyed, wasDestroyed);
    }
    return *this;
}

Shader::Shader(Shader&& other) noexcept
{
    std::swap(other.mID, mID);
    std::swap(other.wasDestroyed, wasDestroyed);
}

Shader::~Shader()
{
    destroy();
    mID = 0;
}

void Shader::setSource(const std::string& src)
{
    assert(!wasDestroyed);
    const GLchar *srcData = src.data();
    GLint size = static_cast<GLint>(src.size());

    glShaderSource(mID, 1, &srcData, &size);
}

void Shader::setSourceFile(const std::string& filename)
{
    assert(!wasDestroyed);
    std::ifstream file(filename);
    if(!file.is_open())
        throw std::runtime_error(std::string("Can't open file: ") + filename);

    setSourceFile(file);
}

void Shader::setSourceFile(std::ifstream& file)
{
    assert(!wasDestroyed);
    file.seekg(0, std::ios::end);
    auto size = file.tellg();
    std::string buffer(static_cast<std::size_t>(size), '\0');
    file.seekg(0);
    file.read(&buffer[0], size);

    setSource(buffer);
}

bool Shader::compile()
{
    assert(!wasDestroyed);
    glCompileShader(mID);
    return isCompileOk();
}

void Shader::destroy()
{
    if(!wasDestroyed)
    {
        glDeleteShader(mID);
        wasDestroyed = true;
    }
}

std::string Shader::infoLog() const
{
    assert(!wasDestroyed);
    GLint logMaxLength;
    glGetShaderiv(mID, GL_INFO_LOG_LENGTH, &logMaxLength);
    std::string ret(logMaxLength, '\0');

    GLsizei length;
    glGetShaderInfoLog(mID, static_cast<GLsizei>(ret.size()), &length, &ret[0]);
    ret.resize(length);
    return ret;
}

bool Shader::isCompileOk() const
{
    assert(!wasDestroyed);
    GLboolean ret = GL_FALSE;
    glGetShaderiv(mID, GL_COMPILE_STATUS, &ret);
    return (ret == GL_TRUE);
}

bool Shader::isDeleteFlagged() const
{
    assert(!wasDestroyed);
    GLboolean ret = GL_FALSE;
    glGetShaderiv(mID, GL_DELETE_STATUS, &ret);
    return (ret == GL_TRUE);
}

gl33core::GLenum Shader::type() const
{
    assert(!wasDestroyed);
    GLenum ret = GL_INVALID_ENUM;
    glGetShaderiv(mID, GL_SHADER_TYPE, &ret);
    return ret;
}

gl33core::GLuint Shader::id() const
{
    return mID;
}



} // namespace GL
} // namespace CGGD
