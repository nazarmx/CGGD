/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once

#include <functional>
#include <string>
#include <utility>

struct GLFWwindow;
namespace CGGD {
class Window final
{
public:
    explicit Window(GLFWwindow* ptr);
    Window(const std::string& name, int w, int h);
    Window(const Window &) = delete;
    Window& operator=(const Window&) = delete;
    Window(Window && other) noexcept;
    Window& operator=(Window && other) noexcept;
    ~Window();

    void setTitle(const std::string& title);
    void makeContextCurrent() const;
    bool shouldClose() const;
    void swapBuffers() const;
    void close(bool useCallback = false);

    using ErrorCallback = std::function<void(int, const std::string&)>;
    static void setErrorCallback(const ErrorCallback& f);
    using PosCallback = std::function<void(int, int)>;
    void setPosCallback(const PosCallback& f);
    using ResizeCallback = std::function<void(int, int)>;
    void setResizeCallback(const ResizeCallback& f);
    using RefreshCallback = std::function<void()>;
    void setRefreshCallback(const RefreshCallback& f);
    using CloseCallback = std::function<void()>;
    void setCloseCallback(const CloseCallback& f);
    using FocusCallback = std::function<void(bool)>;
    void setFocusCallback(const FocusCallback& f);
    using IconifyCallback = std::function<void(bool)>;
    void setIconifyCallback(const IconifyCallback& f);
    using ClientResizeCallback = std::function<void(int, int)>;
    void setClientResizeCallback(const ClientResizeCallback& f);

    using KeyCallback = std::function<void(int, int, bool, int)>;
    void setKeyCallback(const KeyCallback& f);
    using CharacterCallback = std::function<void(unsigned int)>;
    void setCharacterCallback(const CharacterCallback& f);
    using CharacterModsCallback = std::function<void(unsigned int, int)>;
    void setCharacterModsCallback(const CharacterModsCallback& f);

    using MousePosCallback = std::function<void(double, double)>;
    void setMousePosCallback(const MousePosCallback& f);
    using MouseButtonCallback = std::function<void(int, bool, int)>;
    void setMouseButtonCallback(const MouseButtonCallback& f);
    using ScrollCallback = std::function<void(double, double)>;
    void setScrollCallback(const ScrollCallback& f);

    static void setSwapInterval(int interval);
    static void pollEvents();
    static void waitEvents();
    static void postEmptyEvent();

    std::pair<int, int> size() const;
    std::pair<int, int> clientSize() const;
    int width() const;
    int height() const;
    int clientWidth() const;
    int clientHeight() const;
    float clientAspectRatio() const;

    std::pair<double, double> currentMousePos() const;

    GLFWwindow* nativeHandle() const;

private:
    PosCallback mPositionChangedCallback;
    ResizeCallback mSizeChangedCallback;
    RefreshCallback mRefreshCallback;
    CloseCallback mCloseCallback;
    FocusCallback mFocusCallback;
    IconifyCallback mIconifyCallback;
    ClientResizeCallback mClientResizeCallback;

    KeyCallback mKeyCallback;
    CharacterCallback mCharacterCallback;
    CharacterModsCallback mCharacterModsCallback;

    MousePosCallback mMousePosCallback;
    MouseButtonCallback mMouseButtonCallback;
    ScrollCallback mScrollCallback;

    void setupCallbacks();
    static ErrorCallback sErrorCallback;
    static void GLFWInternalErrorCallback(int code, const char *msg);
    static void GLFWInternalPosCallback(GLFWwindow* window, int x, int y);
    static void GLFWInternalResizeCallback(GLFWwindow* window, int w, int h);
    static void GLFWInternalRefreshCallback(GLFWwindow* window);
    static void GLFWInternalCloseCallback(GLFWwindow* window);
    static void GLFWInternalFocusCallback(GLFWwindow* window, int val);
    static void GLFWInternalIconifyCallback(GLFWwindow* window, int val);
    static void GLFWInternalClientResizeCallback(GLFWwindow* window, int w, int h);

    static void GLFWInternalKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void GLFWInternalCharCallback(GLFWwindow* window, unsigned int val);
    static void GLFWInternalCharModsCallback(GLFWwindow* window, unsigned int val, int mods);
    static void GLFWInternalMousePosCallback(GLFWwindow* window, double x, double y);
    static void GLFWInternalMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
    static void GLFWInternalScrollCallback(GLFWwindow* window, double xoffset, double yoffset);

    GLFWwindow* pWindow;
};

} // namespace CGGD
