/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Window.hpp"
#include "Sprite.hpp"
#include "GLWrapper/Buffer.hpp"
#include "GLWrapper/Texture.hpp"
#include "GLWrapper/VertexArrayObject.hpp"
#include "GLWrapper/Shader.hpp"
#include "GLWrapper/ShaderProgram.hpp"
#include "GLWrapper/Exception.hpp"
#include "GLWrapper/Debug.hpp"

#if defined(_MSC_VER)
#pragma warning(push, 0)
#endif
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#include <glbinding/CallbackMask.h>
#include <glbinding/callbacks.h>
#include <glbinding/Binding.h>
#include <glbinding/logging.h>
#include <glbinding/ContextInfo.h>
#include <glbinding/Meta.h>
#include <glbinding/gl33core/gl.h>
#include <glbinding/gl33ext/gl.h>
using namespace gl33core;

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <chrono>
#include <cmath>
#include <string>
#include <utility>
#include <random>
#include <cstdint>
#include <thread>
#include <atomic>
#include <functional>

namespace {
constexpr auto WINDOW_WIDTH = 1024;
constexpr auto WINDOW_HEIGHT = 768;
constexpr auto TITLE_UPDATE_TIMEOUT = 100;
constexpr auto SPRITE_COUNT_UPDATE_TIMEOUT = 100;

constexpr auto SPRITE_SPAWN_TIMEOUT = 100;

std::atomic_uintmax_t titleUsecPerFrame;
std::uintmax_t titleSpriteCount;
std::atomic_bool titleUpdatePending;
}
void drawThread(CGGD::Window & w)
{
    w.makeContextCurrent();
    glbinding::Binding::initialize(false);
    w.setSwapInterval(1);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    CGGD::GL::Buffer vbo;
    struct Vertex {
        GLfloat x, y;
        GLfloat u,v;
        GLubyte r,g,b,a;
    };
    {
        Vertex vertices[] = {
            Vertex{-0.5f, -0.5f, 0.0f, 0.0f, 0xFF, 0x00, 0x00, 0xFF},
            Vertex{+0.5f, -0.5f, 1.0f, 0.0f, 0x00, 0xFF, 0x00, 0xFF},
            Vertex{-0.5f, +0.5f, 0.0f, 1.0f, 0x00, 0x00, 0xFF, 0xFF},
            Vertex{+0.5f, +0.5f, 1.0f, 1.0f, 0xFF, 0xFF, 0xFF, 0xFF}
        };
        vbo.setData(vertices);
    }
    CGGD::GL::Buffer ibo;
    {
        GLubyte indices[] = {
            0, 1, 2,
            1, 2, 3
        };
        ibo.setData(indices);
    }

    CGGD::GL::ShaderProgram program;

    CGGD::GL::Shader vertexShader(GL_VERTEX_SHADER);
    vertexShader.setSourceFile("data/shaders/1.vert");
    vertexShader.compile();
    {
        auto infoLog = vertexShader.infoLog();
        if(!infoLog.empty())
            std::cerr << "Vertex shader infolog: "<< infoLog << '\n';
    }

    program.attachShader(vertexShader);

    CGGD::GL::Shader fragmentShader(GL_FRAGMENT_SHADER);
    fragmentShader.setSourceFile("data/shaders/1.frag");
    fragmentShader.compile();
    {
        auto infoLog = fragmentShader.infoLog();
        if(!infoLog.empty())
            std::cerr << "Fragment shader infolog: "<< infoLog << '\n';
    }

    program.attachShader(fragmentShader);

    program.link();
    {
        auto infoLog = program.infoLog();
        if(!infoLog.empty())
            std::cerr << "Shader program infolog: "<< infoLog << '\n';
    }
    vertexShader.destroy();
    program.detachShader(vertexShader);
    fragmentShader.destroy();
    program.detachShader(fragmentShader);

    w.setErrorCallback(
       [](int code, const std::string& msg)
       {
            std::cerr << "[GLFW Error](" << code << "): " << msg << '\n';
            std::terminate();
       }
    );

#ifndef NDEBUG
    CGGD::GL::Debug::setCallback(
        [](GLenum source, GLenum type, GLuint id, GLenum severity, const std::string& msg, const void *data)
        {
            std::cerr << "###########********  Debug Callback Start ********###########\n"
                      << "source=" << glbinding::Meta::getString(source) << '\n'
                      << "type=" << glbinding::Meta::getString(type) << '\n'
                      << "id=" << id << '\n'
                      << "severity=" << glbinding::Meta::getString(severity) << '\n'
                      << "length=" << msg.size() << '\n'
                      << "msg=" << msg << '\n'
                      << "data=" << data << '\n'
                      << "###########********  Debug Callback End ********###########\n";
        }
    );
    glbinding::setCallbackMaskExcept(glbinding::CallbackMask::After, { "glGetError" });
    glbinding::setAfterCallback([](const glbinding::FunctionCall &call)
    {
      if (glGetError() != GL_NO_ERROR)
        throw CGGD::GL::Exception(call);
    });
#endif

    const float aspect = w.clientAspectRatio();
    const glm::mat4 projection = glm::ortho(-aspect, aspect, -1.0f, 1.0f);

    CGGD::GL::VertexArrayObject vao;
    using VAOBinder = CGGD::GL::VertexArrayObject::Binder;
    using VBOBinder = CGGD::GL::Buffer::Binder;
    {
        VAOBinder vaoBinder(vao);
        ibo.bind(GL_ELEMENT_ARRAY_BUFFER);


        VBOBinder vboBinder(vbo);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex),
                          reinterpret_cast<void*>(offsetof(Vertex, r)));
    }
    {
        CGGD::GL::ShaderProgram::Binder programBinder(program);
        auto projectionLoc = program.uniformLocation("Projection");
        glUniformMatrix4fv(projectionLoc, 1, false, glm::value_ptr(projection));
    }

    CGGD::GL::Texture test;
    test.loadFromFile("data/textures/awesomeface.dds");

    glEnable(GL_PROGRAM_POINT_SIZE);
    std::vector<CGGD::Sprite> sprites;

#if defined(__MINGW32__) || defined (__MINGW64__)
    std::mt19937_64 gen(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
#else
    std::mt19937_64 gen(std::random_device{}());
#endif

    auto randomColor = [&](){
        static std::uniform_int_distribution<unsigned short> dist(0, 0xFF);
        return glm::u8vec4{
            static_cast<glm::u8>(dist(gen)), // Red
            static_cast<glm::u8>(dist(gen)), // Green
            static_cast<glm::u8>(dist(gen)), // Blue
            0xFF // Alpha
        };
    };

    CGGD::Sprite::updateProjectionMatrix(projection);


    auto randomPosition = [&](){
        static std::uniform_real_distribution<float> distPosX(-aspect, aspect);
        static std::uniform_real_distribution<float> distPosY(-1, 1);
        return glm::vec2 {
            distPosX(gen), // X
            distPosY(gen), // Y
        };
    };

    std::uniform_real_distribution<float> distSize(50.0f, 200.0f);    

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    const auto timepointBeforeLoopStart = std::chrono::steady_clock::now();
    auto spriteSpawnLastTime = timepointBeforeLoopStart;
    auto timepointLastTitleUpdate = timepointBeforeLoopStart;
    auto timepointLastCountUpdate = timepointBeforeLoopStart;

    while(!w.shouldClose())
    {
        const auto timepointFrameStart = std::chrono::steady_clock::now();
        const auto uSecSinceStart = std::chrono::duration_cast<std::chrono::microseconds>(timepointFrameStart - timepointBeforeLoopStart).count();

        const auto mSecSinceLastSpawn = std::chrono::duration_cast<std::chrono::milliseconds>(timepointFrameStart - spriteSpawnLastTime).count();
        if(mSecSinceLastSpawn >= SPRITE_SPAWN_TIMEOUT)
        {
            sprites.emplace_back(CGGD::Sprite::SpriteData
                                 {
                                    randomPosition(),
                                    distSize(gen), // size in pixels
                                    0.0f, // angle
                                    randomColor()
                                 },
                                 10, // z-Index TODO
                                 test.id()
            );
            //sprites.back().setPosition({0.5f, 0.5f});
            //sprites.back().setColor({0xFF, 0, 0, 0xFF});
            //sprites.back().setSize(50.0f);
            //sprites.back().setAngle(50.0f);

            spriteSpawnLastTime = timepointFrameStart;
        }

        glClear(GL_COLOR_BUFFER_BIT);
        {
            VAOBinder binder(vao);
            CGGD::GL::ShaderProgram::Binder programBinder(program);
            auto timeMsecLoc = program.uniformLocation("timeUsec");
            glUniform1ui(timeMsecLoc, static_cast<GLuint>(uSecSinceStart));
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, nullptr);
        }
        CGGD::Sprite::updateTime(static_cast<GLuint>(uSecSinceStart));

        for(auto& sprite: sprites)
            sprite.draw();

        w.swapBuffers();

        const auto timepointFrameEnd = std::chrono::steady_clock::now();
        const auto uSecFrameTime = std::chrono::duration_cast<std::chrono::microseconds>(timepointFrameEnd - timepointFrameStart).count();
        titleUsecPerFrame = uSecFrameTime;

        const auto mSecSinceLastTitleUpdate = std::chrono::duration_cast<std::chrono::milliseconds>(timepointFrameEnd - timepointLastTitleUpdate).count();
        if(mSecSinceLastTitleUpdate >= TITLE_UPDATE_TIMEOUT)
        {
            if(!titleUpdatePending)
            {
                w.postEmptyEvent();
                titleUpdatePending = true;
            }

            timepointLastTitleUpdate = timepointFrameEnd;
        }

        titleSpriteCount = sprites.size();
        const auto mSecSinceLastCountUpdate = std::chrono::duration_cast<std::chrono::milliseconds>(timepointFrameEnd - timepointLastCountUpdate).count();
        if(mSecSinceLastCountUpdate >= SPRITE_COUNT_UPDATE_TIMEOUT)
        {
            if(!titleUpdatePending)
            {
                w.postEmptyEvent();
                titleUpdatePending = true;
            }

            timepointLastCountUpdate = timepointFrameEnd;
        }


    }
}

int main()
{
    CGGD::Window w {"CGGD", WINDOW_WIDTH, WINDOW_HEIGHT};

    std::thread th(std::bind(&drawThread, std::ref(w)));

    while(!w.shouldClose())
    {
        CGGD::Window::waitEvents();

        if(titleUpdatePending.load())
        {
            const double titleValue = titleUsecPerFrame.load() / 1e3;
            using namespace std::string_literals;
            w.setTitle("CGGD - "s + std::to_string(titleValue) + " msec/frame (~"s + std::to_string(1e3/titleValue) + " FPS) - "s +
                       std::to_string(titleSpriteCount) + " sprites");
            titleUpdatePending = false;
        }
    }
    th.join();
}
