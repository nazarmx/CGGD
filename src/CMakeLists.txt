#[[
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
]]#
add_subdirectory(GLWrapper)

list(APPEND SRC_LIST
    main.cpp
    Window.cpp
    GLFWException.cpp
    Sprite.cpp
)
add_executable(${PROJECT_NAME} WIN32 ${SRC_LIST})
if(MSVC)
    target_link_libraries(${PROJECT_NAME} PRIVATE -entry:mainCRTStartup)
endif()
target_compile_definitions(${PROJECT_NAME} PRIVATE "GLFW_INCLUDE_NONE" "GLM_META_PROG_HELPERS")
target_link_libraries(${PROJECT_NAME} PRIVATE glfw GLWrapper glbinding::glbinding)
if(MINGW AND (USE_CPACK OR CI))
    target_link_libraries(${PROJECT_NAME} PRIVATE -Wl,-Bstatic -lstdc++ -lpthread -Wl,-Bdynamic -static-libgcc -static-libstdc++)
endif()

set(EXECUTABLE_NAME "${PROJECT_NAME}")
set(RUNTIME_DIR ".")

install(TARGETS ${EXECUTABLE_NAME} RUNTIME DESTINATION ${RUNTIME_DIR})
install(DIRECTORY "${PROJECT_SOURCE_DIR}/data" DESTINATION ${RUNTIME_DIR})
if(USE_CPACK OR CI)
    list(APPEND CPACK_PACKAGE_EXECUTABLES "${EXECUTABLE_NAME}" "CGGD - nazar554")
    set(CPACK_PACKAGE_EXECUTABLES ${CPACK_PACKAGE_EXECUTABLES} PARENT_SCOPE)
    install(CODE "
        include(BundleUtilities)
        fixup_bundle(\"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${RUNTIME_DIR}/${EXECUTABLE_NAME}${CMAKE_EXECUTABLE_SUFFIX}\" \"\" \"\")
        file(GLOB dynamic_libs \"\${CMAKE_INSTALL_PREFIX}/${RUNTIME_DIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}*\")
        message(STATUS \"GLOB: \${dynamic_libs}\")
        foreach(dll IN LISTS dynamic_libs)
            if(\"${CMAKE_STRIP}\")
                execute_process(COMMAND ${CMAKE_STRIP} \"-s\" \${dll})
            endif()
        endforeach(dll)
    ")
endif()
cotire(${PROJECT_NAME})
