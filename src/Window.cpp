/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Window.hpp"
#include "GLFWException.hpp"

#include <GLFW/glfw3.h>
#include <cstdlib>
#include <exception>
#include <utility>
#include <cassert>
namespace CGGD {

Window::ErrorCallback Window::sErrorCallback;

Window::Window(GLFWwindow* ptr) :
    pWindow{ptr}
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window ctor called with ptr = nullptr");
    glfwSetWindowUserPointer(pWindow, this);

    setupCallbacks();
}

Window::Window(const std::string& name, int w, int h) :
    pWindow{nullptr}
{
    static bool needsInitialize = true;
    if(needsInitialize)
    {
        glfwSetErrorCallback(&GLFWInternalErrorCallback);
        int ret = glfwInit();
        if(!ret)
        {
            throw GLFW::Exception("glfwInit() failed", ret);
        }
        else
        {
            needsInitialize = false;
            std::atexit(&glfwTerminate);
        }
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_DOUBLEBUFFER, 1);
    glfwWindowHint(GLFW_RESIZABLE, 0);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
#endif

    pWindow = glfwCreateWindow(w, h, name.c_str(), nullptr, nullptr);
    if(pWindow == nullptr)
        throw GLFW::Exception("glfwCreateWindow() failed");

    setupCallbacks();
}

Window::Window(Window&& other) noexcept :
    pWindow{nullptr}
{
    *this = std::move(other);
}

Window& Window::operator=(Window&& other) noexcept
{
    if(this == &other)
        return *this;

    if(pWindow != nullptr)
    {
        glfwDestroyWindow(pWindow);
        pWindow = nullptr;
    }

    std::swap(pWindow, other.pWindow);
    std::swap(mPositionChangedCallback, other.mPositionChangedCallback);
    std::swap(mSizeChangedCallback, other.mSizeChangedCallback);
    std::swap(mRefreshCallback, other.mRefreshCallback);
    std::swap(mCloseCallback, other.mCloseCallback);
    std::swap(mFocusCallback, other.mFocusCallback);
    std::swap(mIconifyCallback, other.mIconifyCallback);
    std::swap(mClientResizeCallback, other.mClientResizeCallback);

    if(pWindow)
        glfwSetWindowUserPointer(pWindow, this);
    return *this;
}

Window::~Window()
{
    if(pWindow)
        glfwDestroyWindow(pWindow);
}

void Window::setTitle(const std::string& title)
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::setTitle() called with closed window");
    glfwSetWindowTitle(pWindow, title.c_str());
}

void Window::makeContextCurrent() const
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::makeContextCurrent() called with closed window");
    glfwMakeContextCurrent(pWindow);
}

bool Window::shouldClose() const
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::shouldClose() called with closed window");
    return (glfwWindowShouldClose(pWindow) == 1);
}

void Window::swapBuffers() const
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::swapBuffers() called with closed window");
    glfwSwapBuffers(pWindow);
}

void Window::close(bool useCallback)
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::close() called with closed window");
    if(useCallback)
        mCloseCallback();
    glfwDestroyWindow(pWindow);
    pWindow = nullptr;
}

void Window::setErrorCallback(const Window::ErrorCallback& f)
{
    sErrorCallback = f;
}

void Window::setPosCallback(const Window::PosCallback& f)
{
    mPositionChangedCallback = f;
}

void Window::setResizeCallback(const Window::ResizeCallback& f)
{
    mSizeChangedCallback = f;
}

void Window::setRefreshCallback(const Window::RefreshCallback& f)
{
    mRefreshCallback = f;
}

void Window::setCloseCallback(const Window::CloseCallback& f)
{
    mCloseCallback = f;
}

void Window::setFocusCallback(const Window::FocusCallback& f)
{
    mFocusCallback = f;
}

void Window::setClientResizeCallback(const Window::ClientResizeCallback& f)
{
    mClientResizeCallback = f;
}

void Window::setKeyCallback(const Window::KeyCallback& f)
{
    mKeyCallback = f;
}

void Window::setCharacterCallback(const Window::CharacterCallback& f)
{
    mCharacterCallback = f;
}

void Window::setCharacterModsCallback(const Window::CharacterModsCallback& f)
{
    mCharacterModsCallback = f;
}

void Window::setMousePosCallback(const Window::MousePosCallback& f)
{
    mMousePosCallback = f;
}

void Window::setMouseButtonCallback(const Window::MouseButtonCallback& f)
{
    mMouseButtonCallback = f;
}

void Window::setScrollCallback(const Window::ScrollCallback& f)
{
    mScrollCallback = f;
}

void Window::setSwapInterval(int interval)
{
    glfwSwapInterval(interval);
}

void Window::pollEvents()
{
    glfwPollEvents();
}

void Window::waitEvents()
{
    glfwWaitEvents();
}

void Window::postEmptyEvent()
{
    glfwPostEmptyEvent();
}

std::pair<int, int> Window::size() const
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::size() called with closed window");
    std::pair<int, int> ret;
    glfwGetWindowSize(pWindow, &ret.first, &ret.second);
    return ret;
}

std::pair<int, int> Window::clientSize() const
{
    if(pWindow == nullptr)
        throw GLFW::Exception("Window::clientSize() called with closed window");
    std::pair<int, int> ret;
    glfwGetFramebufferSize(pWindow, &ret.first, &ret.second);
    return ret;
}

int Window::width() const
{
    return size().first;
}

int Window::height() const
{
    return size().second;
}

int Window::clientWidth() const
{
    return clientSize().first;
}

int Window::clientHeight() const
{
    return clientSize().second;
}

float Window::clientAspectRatio() const
{
    auto size = clientSize();
    return static_cast<float>(size.first) / static_cast<float>(size.second);
}

std::pair<double, double> Window::currentMousePos() const
{
    std::pair<double, double> ret;
    glfwGetCursorPos(pWindow, &ret.first, &ret.second);
    return ret;
}

GLFWwindow* Window::nativeHandle() const
{
    return pWindow;
}

void Window::setupCallbacks()
{
    glfwSetWindowUserPointer(pWindow, this);
    glfwSetWindowPosCallback(pWindow, &GLFWInternalPosCallback);
    glfwSetWindowSizeCallback(pWindow, &GLFWInternalResizeCallback);
    glfwSetWindowRefreshCallback(pWindow, &GLFWInternalRefreshCallback);
    glfwSetWindowCloseCallback(pWindow, &GLFWInternalCloseCallback);
    glfwSetWindowFocusCallback(pWindow, &GLFWInternalFocusCallback);
    glfwSetWindowIconifyCallback(pWindow, &GLFWInternalIconifyCallback);
    glfwSetFramebufferSizeCallback(pWindow, &GLFWInternalClientResizeCallback);

    glfwSetKeyCallback(pWindow, &GLFWInternalKeyCallback);
    glfwSetCharCallback(pWindow, &GLFWInternalCharCallback);
    glfwSetCharModsCallback(pWindow, &GLFWInternalCharModsCallback);

    glfwSetCursorPosCallback(pWindow, &GLFWInternalMousePosCallback);
    glfwSetMouseButtonCallback(pWindow, &GLFWInternalMouseButtonCallback);
    glfwSetScrollCallback(pWindow, &GLFWInternalScrollCallback);
}

void Window::GLFWInternalErrorCallback(int code, const char* msg)
{
    if(sErrorCallback)
        sErrorCallback(code, std::string{msg});
}

void Window::GLFWInternalPosCallback(GLFWwindow* window, int x, int y)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mPositionChangedCallback)
        instancePtr->mPositionChangedCallback(x, y);
}

void Window::GLFWInternalResizeCallback(GLFWwindow* window, int w, int h)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mSizeChangedCallback)
        instancePtr->mSizeChangedCallback(w, h);
}

void Window::GLFWInternalRefreshCallback(GLFWwindow* window)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mRefreshCallback)
        instancePtr->mRefreshCallback();
}

void Window::GLFWInternalCloseCallback(GLFWwindow* window)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mCloseCallback)
        instancePtr->mCloseCallback();
}

void Window::GLFWInternalFocusCallback(GLFWwindow* window, int val)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mFocusCallback)
        instancePtr->mFocusCallback(val == 1);
}

void Window::GLFWInternalIconifyCallback(GLFWwindow* window, int val)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mIconifyCallback)
        instancePtr->mIconifyCallback(val == 1);
}

void Window::GLFWInternalClientResizeCallback(GLFWwindow* window, int w, int h)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mClientResizeCallback)
        instancePtr->mClientResizeCallback(w, h);
}

void Window::GLFWInternalKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mKeyCallback)
        instancePtr->mKeyCallback(key, scancode, (action == GLFW_PRESS), mods);
}

void Window::GLFWInternalCharCallback(GLFWwindow* window, unsigned int val)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mCharacterCallback)
        instancePtr->mCharacterCallback(val);
}

void Window::GLFWInternalCharModsCallback(GLFWwindow* window, unsigned int val, int mods)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mCharacterModsCallback)
        instancePtr->mCharacterModsCallback(val, mods);
}

void Window::GLFWInternalMousePosCallback(GLFWwindow* window, double x, double y)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mMousePosCallback)
        instancePtr->mMousePosCallback(x, y);
}

void Window::GLFWInternalMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mMouseButtonCallback)
        instancePtr->mMouseButtonCallback(button, (action == GLFW_PRESS), mods);
}

void Window::GLFWInternalScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    void *ptr = glfwGetWindowUserPointer(window);
    assert(ptr);
    Window* instancePtr = static_cast<Window*>(ptr);
    if(instancePtr->mScrollCallback)
        instancePtr->mScrollCallback(xoffset, yoffset);
}
} // namespace CGGD
