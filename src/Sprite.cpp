/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "Sprite.hpp"
#include <map>
#if defined(_MSC_VER)
#pragma warning(push, 0)
#endif
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#if defined(_MSC_VER)
#pragma warning(pop)
#endif
#include <glbinding/gl33core/gl.h>
using namespace gl33core;
namespace {
const std::size_t INITIAL_BUFFER_SIZE = 100;
}

namespace CGGD {
std::multimap<Sprite*, std::size_t> Sprite::sIndexMap;
std::unique_ptr<CGGD::GL::Buffer> Sprite::sBufferPtr;
std::unique_ptr<CGGD::GL::ShaderProgram> Sprite::sProgramPtr;
std::unique_ptr<CGGD::GL::VertexArrayObject> Sprite::sVAO;

Sprite::Sprite(const SpriteData& data, std::int64_t z, gl33core::GLuint texID) :
    zIndex{z},
    mTextureID{texID}
{
    if(!sVAO)
        setupVao();
    CGGD::GL::VertexArrayObject::Binder binder(*sVAO);

    if( !sBufferPtr || ( sIndexMap.begin()->first != nullptr) )
    {        
        std::size_t currentSize = 0;
        if(sBufferPtr)
            currentSize = sBufferPtr->sizeBytes()/sizeof(SpriteData);
        const std::size_t newSize = currentSize + INITIAL_BUFFER_SIZE;

        // we need a new buffer
        auto newBufferPtr = std::make_unique<GL::Buffer>();
        newBufferPtr->setData<SpriteData>(nullptr, newSize, GL_COPY_WRITE_BUFFER, GL_DYNAMIC_COPY);
        auto it = sIndexMap.begin();
        for(std::size_t i = newSize; i != currentSize; --i)
            it = sIndexMap.emplace_hint(it, nullptr, i-1);

        // copy the old buffer into it
        if(sBufferPtr)
            sBufferPtr->copyTo(*newBufferPtr);
        sBufferPtr.swap(newBufferPtr);
        setupAttribPointers();
    }

    assert(!sIndexMap.empty());
    auto freeSpotIterator = sIndexMap.begin();
    assert(freeSpotIterator->first == nullptr);
    mBufferIndex = freeSpotIterator->second;
    sIndexMap.erase(freeSpotIterator);
    sIndexMap.emplace(this, mBufferIndex);

    sBufferPtr->updateData(mBufferIndex, &data, 1);
}

Sprite::Sprite(Sprite&& other) noexcept
{
    std::swap(zIndex, other.zIndex);
    std::swap(mTextureID, other.mTextureID);

    std::swap(mBufferIndex, other.mBufferIndex);
    sIndexMap.erase(&other);
    sIndexMap.emplace(this, mBufferIndex);
}

Sprite& Sprite::operator=(Sprite&& other)
{
    if(this == &other)
        return *this;
    std::swap(zIndex, other.zIndex);
    std::swap(mTextureID, other.mTextureID);
    auto myIt = sIndexMap.find(this);
    if(myIt != sIndexMap.end()) // we are holding a index
    {
        sIndexMap.erase(myIt);
        sIndexMap.emplace(nullptr, mBufferIndex);
    }

    sIndexMap.erase(&other);
    std::swap(mBufferIndex, other.mBufferIndex);
    sIndexMap.emplace(this, mBufferIndex);

    return *this;
}

Sprite::~Sprite()
{
    auto myIt = sIndexMap.find(this);
    if(myIt != sIndexMap.end()) // we are holding a index
    {
        sIndexMap.erase(this);
        sIndexMap.emplace(nullptr, mBufferIndex);
    }
    if(sIndexMap.upper_bound(nullptr) == sIndexMap.end())
    {
        sBufferPtr.reset();
        sProgramPtr.reset();
        sVAO.reset();
        sIndexMap.clear();
    }
}

void Sprite::draw() const
{
    if(!sProgramPtr)
        setupShaderProgram();
    CGGD::GL::ShaderProgram::Binder programBinder(*sProgramPtr);
    CGGD::GL::VertexArrayObject::Binder binder(*sVAO);

    glBindTexture(GL_TEXTURE_2D, mTextureID);
    glDrawArrays(GL_POINTS, static_cast<GLint>(mBufferIndex), 1);
}

void Sprite::setPosition(glm::vec2 pos)
{
    assert(sBufferPtr);
    const std::size_t offset = mBufferIndex*sizeof(SpriteData) + offsetof(SpriteData, pos);
    sBufferPtr->updateDataRaw(offset, &pos, sizeof(pos));
}

void Sprite::setColor(glm::u8vec4 color)
{
    assert(sBufferPtr);
    const std::size_t offset = mBufferIndex*sizeof(SpriteData) + offsetof(SpriteData, color);
    sBufferPtr->updateDataRaw(offset, &color, sizeof(color));
}

void Sprite::setAngle(float angleRadians)
{
    assert(sBufferPtr);
    const std::size_t offset = mBufferIndex*sizeof(SpriteData) + offsetof(SpriteData, angleRadians);
    sBufferPtr->updateDataRaw(offset, &angleRadians, sizeof(angleRadians));
}

void Sprite::setSize(float px)
{
    assert(sBufferPtr);
    const std::size_t offset = mBufferIndex*sizeof(SpriteData) + offsetof(SpriteData, sizePixels);
    sBufferPtr->updateDataRaw(offset, &px, sizeof(px));
}

void Sprite::updateData(const Sprite::SpriteData& data)
{
    assert(sBufferPtr);
    sBufferPtr->updateData(mBufferIndex, &data, 1);
}

void Sprite::updateProjectionMatrix(const glm::mat4& mat)
{
    if(!sProgramPtr)
        setupShaderProgram();
    CGGD::GL::ShaderProgram::Binder programBinder(*sProgramPtr);

    auto projectionLoc = sProgramPtr->uniformLocation("Projection");
    glUniformMatrix4fv(projectionLoc, 1, false, glm::value_ptr(mat));
}

void Sprite::updateTime(gl33core::GLuint timeUsec)
{
    if(!sProgramPtr)
        setupShaderProgram();
    CGGD::GL::ShaderProgram::Binder programBinder(*sProgramPtr);
    auto timeMsecLoc = sProgramPtr->uniformLocation("timeUsec");
    glUniform1ui(timeMsecLoc, timeUsec);
}

void Sprite::setupShaderProgram()
{
    assert(sProgramPtr == nullptr);

    sProgramPtr = std::make_unique<GL::ShaderProgram>();
    CGGD::GL::Shader vertexShader(GL_VERTEX_SHADER);
    vertexShader.setSourceFile("data/shaders/Sprite.vert");
    vertexShader.compile();
    {
        auto infoLog = vertexShader.infoLog();
        if(!infoLog.empty())
            std::cerr << "Vertex shader infolog: "<< infoLog << '\n';
    }

    sProgramPtr->attachShader(vertexShader);

    CGGD::GL::Shader fragmentShader(GL_FRAGMENT_SHADER);
    fragmentShader.setSourceFile("data/shaders/Sprite.frag");
    fragmentShader.compile();
    {
        auto infoLog = fragmentShader.infoLog();
        if(!infoLog.empty())
            std::cerr << "Fragment shader infolog: "<< infoLog << '\n';
    }

    sProgramPtr->attachShader(fragmentShader);

    sProgramPtr->link();
    {
        auto infoLog = sProgramPtr->infoLog();
        if(!infoLog.empty())
            std::cerr << "Shader program infolog: "<< infoLog << '\n';
    }

    vertexShader.destroy();
    sProgramPtr->detachShader(vertexShader);

    fragmentShader.destroy();
    sProgramPtr->detachShader(fragmentShader);
}

void Sprite::setupVao()
{
    sVAO = std::make_unique<GL::VertexArrayObject>();
}

void Sprite::setupAttribPointers()
{
    if(!sVAO)
        setupVao();
    CGGD::GL::VertexArrayObject::Binder binder(*sVAO);
    CGGD::GL::Buffer::Binder vboBinder(*sBufferPtr);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, decltype(SpriteData::pos)::components + 2, GL_FLOAT, GL_FALSE, sizeof(SpriteData),
                          reinterpret_cast<void*>(offsetof(SpriteData, pos))
                          );
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, decltype(SpriteData::color)::components, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(SpriteData),
                          reinterpret_cast<void*>(offsetof(SpriteData, color))
                          );
}

gl33core::GLuint Sprite::getTextureID() const
{
    return mTextureID;
}

void Sprite::setTextureID(gl33core::GLuint textureID)
{
    mTextureID = textureID;
}

std::int64_t Sprite::getZIndex() const
{
    return zIndex;
}

void Sprite::setZIndex(int64_t value)
{
    zIndex = value;
}

bool operator<(const CGGD::Sprite& a, const CGGD::Sprite& b)
{
    return (a.zIndex < b.zIndex);
}

} // namespace CGGD
