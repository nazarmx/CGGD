/*
   Copyright 2016 Nazar Mishturak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once
#include <glbinding/gl33core/types.h>
#include "GLWrapper/Shader.hpp"
#include "GLWrapper/ShaderProgram.hpp"
#include "GLWrapper/VertexArrayObject.hpp"
#include "GLWrapper/Buffer.hpp"
#if defined(_MSC_VER)
#pragma warning(push, 0)
#endif
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#include <memory>
#include <cstddef>
#include <limits>
#include <cstdint>
#include <map>

namespace CGGD {
class Sprite
{
public:
    struct SpriteData {
        // stuff these 4 in vec4 for GLSL
        glm::vec2 pos;
        float sizePixels, angleRadians;
        // this will be an another vec4
        glm::u8vec4 color;
    };
    static_assert(offsetof(SpriteData, sizePixels) == sizeof(float)*2, "SpriteData bad padding for size");
    static_assert(offsetof(SpriteData, angleRadians) == sizeof(float)*3, "SpriteData bad padding for angle");
    static_assert(sizeof(SpriteData) == (sizeof(float)*4 + sizeof(glm::u8)*4), "SpriteData bad packing");


    Sprite(const SpriteData& data, std::int64_t z = 0, gl33core::GLuint texID = 0);
    Sprite(Sprite&& other) noexcept;
    Sprite(const Sprite&) = delete;
    Sprite& operator=(const Sprite&) = delete;
    Sprite& operator=(Sprite && other);
    ~Sprite();

    void draw() const;
    void setPosition(glm::vec2 pos);
    void setColor(glm::u8vec4 color);
    void setAngle(float angleRadians);
    void setSize(float px);
    void updateData(const SpriteData& data);


    static void updateProjectionMatrix(const glm::mat4& mat);
    static void updateTime(gl33core::GLuint timeMsec);
    std::int64_t getZIndex() const;
    void setZIndex(std::int64_t value);

    gl33core::GLuint getTextureID() const;
    void setTextureID(gl33core::GLuint textureID);

private:
    static void setupShaderProgram();
    static void setupVao();
    static void setupAttribPointers();

    static std::unique_ptr<CGGD::GL::Buffer> sBufferPtr;
    static std::unique_ptr<CGGD::GL::ShaderProgram> sProgramPtr;
    static std::unique_ptr<CGGD::GL::VertexArrayObject> sVAO;
    static std::multimap<Sprite*, std::size_t> sIndexMap;

    std::size_t mBufferIndex = std::numeric_limits<std::size_t>::max();
    std::int64_t zIndex = 0;
    gl33core::GLuint mTextureID = 0;

    friend bool operator<(const CGGD::Sprite& a, const CGGD::Sprite& b);
};



} // namespace CGGD
